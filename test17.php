<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
  .ui-front {
    z-index: 1060;
}
</style>
    <?php  for($i=0; $i<5 ; $i++){ ?>
    <input type="email" placeholder="" id="skills<?php echo $i;?>" name="to" class="form-control" multiple>
 <script>
    $(function() {
        function split( val ) {
            console.log(val);
            return val.split( /,\s*/ );

        }
        function extractLast( term ) {
            return split( term ).pop();
        }
        
        $( "#skills<?php echo $i;?>" ).bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 1,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                $.getJSON("http://localhost/hestabit/app/skills.php", { term : extractLast( request.term )},response);
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                //console.log(this.value);
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( " ," );
                return false;
            }
        });
    });
    </script>

     <?php 

 } ?>